﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsMenuInicial : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //sai de jogo se apertar pra voltar
        if (Input.GetKeyDown(KeyCode.Escape)){
            Application.Quit();
        }
            
    }

    public void BotaoSairDoJogo()
    {
        PlayerPrefs.DeleteKey("seed");
        Application.Quit();
    }

    // public void ButtonPlayModoDigital()
    // {
    //     Debug.Log("Indo para tela de Seed");
    //     SceneManager.LoadScene("TelaSeed");
    // }

    // public void ButtonPlayModoFisico()
    // {
    //     Debug.Log("Indo para tela de Jogadores");
    //     SceneManager.LoadScene("TelaJogadores");
    // }

    public void ButtonImprimir()
    {
        switch (PlayerPrefs.GetInt("lang"))
        {
            case 0:
                Application.OpenURL("http://www.ldse.ufc.br/fisica_tab.pdf");
                break;
            case 1:
                Application.OpenURL("http://www.ldse.ufc.br/fisica_tab.pdf");
                break;
            case 2:
                Application.OpenURL("http://www.ldse.ufc.br/fisica_tab.pdf");
                break;
            default:
                Application.OpenURL("http://www.ldse.ufc.br/fisica_tab.pdf");
                break;
        }

    }

    public void selectPt(){
        PlayerPrefs.SetInt("lang", 0);
        SceneManager.LoadScene("TelaMenu");
    }

    public void selectEn(){
        PlayerPrefs.SetInt("lang", 1);
        SceneManager.LoadScene("TelaMenu");
    }

    public void selectFr(){
        PlayerPrefs.SetInt("lang", 2);
        SceneManager.LoadScene("TelaMenu");
    }
}
