﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardGenerate : MonoBehaviour
{
    public GameObject textoCarta, imagemCarta;
    public static Card cartaAtual;
    // Start is called before the first frame update
    void Start()
    {
        String codigoCarta = SeedScreen.cartas[PlayerPrefs.GetInt("cartaNumero") - 1];
        //cartaAtual = new Card("A001-058"); // carta com maior texto
        cartaAtual = new Card(codigoCarta);


        Debug.Log("ID atual:" + cartaAtual.GetId() + " | R: " + cartaAtual.GetResposta());

        imagemCarta.GetComponent<Image>().sprite = cartaAtual.GetImagem();
        textoCarta.GetComponent<TextMeshProUGUI>().text = cartaAtual.GetPergunta();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
