﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Game : MonoBehaviour
{
    public GameObject cartaObj, textoCartaObj, imagemCartaObj, buttonsTF, buttonsDiceLuck, botaoResposta, botaoProxCarta, instructionsObj;
    public GameObject buttonSelectTrueObj, buttonSelectFalseObj, highlightObj, imgRespostaObj;

    public GameObject matchInfoObj, imgCertoErradoObj;
    public Texture2D molduraCarta, buttonSelected, buttonNotSelected, highlightAcertou, highlightErrou, imgTrue, imgFalse, imgCerto, imgErrado;
    
    // Start is called before the first frame update
    void Start()
    {
        matchInfoObj.GetComponent<TextMeshProUGUI>().text = "Código da partida: " + PlayerPrefs.GetInt("seed") + " / Carta de número: " + PlayerPrefs.GetInt("cartaNumero");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IniciarJogo(){
        cartaObj.GetComponent<Image>().sprite = Sprite.Create(molduraCarta, new Rect(0.0f, 0.0f, molduraCarta.width, molduraCarta.height), new Vector2(0.5f, 0.5f), 100.0f);
        cartaObj.GetComponent<Button>().interactable = false;
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos respondem";

        textoCartaObj.SetActive(true);
        imagemCartaObj.SetActive(true);
        buttonsTF.SetActive(true);
    }

    public void SelectTrue() {
        PlayerPrefs.SetString("botaoResposta", "true");
        buttonSelectTrueObj.GetComponent<Image>().sprite = Sprite.Create(buttonSelected, new Rect(0.0f, 0.0f, buttonSelected.width, buttonSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
        buttonSelectFalseObj.GetComponent<Image>().sprite = Sprite.Create(buttonNotSelected, new Rect(0.0f, 0.0f, buttonNotSelected.width, buttonNotSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
        botaoResposta.SetActive(true);

        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos checam a resposta";
    }
    public void SelectFalse() {
        PlayerPrefs.SetString("botaoResposta", "false");
        buttonSelectTrueObj.GetComponent<Image>().sprite = Sprite.Create(buttonNotSelected, new Rect(0.0f, 0.0f, buttonNotSelected.width, buttonNotSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
        buttonSelectFalseObj.GetComponent<Image>().sprite = Sprite.Create(buttonSelected, new Rect(0.0f, 0.0f, buttonSelected.width, buttonSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
        botaoResposta.SetActive(true);

        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos checam a resposta";
    }

    public void verResposta() {
        if (CardGenerate.cartaAtual.GetResposta() == true && PlayerPrefs.GetString("botaoResposta") == "true") {
            highlightObj.GetComponent<Image>().sprite = Sprite.Create(highlightAcertou, new Rect(0.0f, 0.0f, highlightAcertou.width, highlightAcertou.height), new Vector2(0.5f, 0.5f), 100.0f);
            instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Acertou!</color>";
            imgRespostaObj.GetComponent<Image>().sprite = Sprite.Create(imgTrue, new Rect(0.0f, 0.0f, imgTrue.width, imgTrue.height), new Vector2(0.5f, 0.5f), 100.0f);
            imgCertoErradoObj.GetComponent<Image>().sprite = Sprite.Create(imgCerto, new Rect(0.0f, 0.0f, imgCerto.width, imgCerto.height), new Vector2(0.5f, 0.5f), 100.0f);
            buttonsDiceLuck.SetActive(true);
        }
        if (CardGenerate.cartaAtual.GetResposta() == false && PlayerPrefs.GetString("botaoResposta") == "false") {
            highlightObj.GetComponent<Image>().sprite = Sprite.Create(highlightAcertou, new Rect(0.0f, 0.0f, highlightAcertou.width, highlightAcertou.height), new Vector2(0.5f, 0.5f), 100.0f);
            instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Acertou!</color>";
            imgRespostaObj.GetComponent<Image>().sprite = Sprite.Create(imgFalse, new Rect(0.0f, 0.0f, imgFalse.width, imgFalse.height), new Vector2(0.5f, 0.5f), 100.0f);
            imgCertoErradoObj.GetComponent<Image>().sprite = Sprite.Create(imgCerto, new Rect(0.0f, 0.0f, imgCerto.width, imgCerto.height), new Vector2(0.5f, 0.5f), 100.0f);
            buttonsDiceLuck.SetActive(true);
        }

        if (CardGenerate.cartaAtual.GetResposta() == true && PlayerPrefs.GetString("botaoResposta") == "false") {
            highlightObj.GetComponent<Image>().sprite = Sprite.Create(highlightErrou, new Rect(0.0f, 0.0f, highlightErrou.width, highlightErrou.height), new Vector2(0.5f, 0.5f), 100.0f);
            instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Errou!</color>";
            imgRespostaObj.GetComponent<Image>().sprite = Sprite.Create(imgTrue, new Rect(0.0f, 0.0f, imgTrue.width, imgTrue.height), new Vector2(0.5f, 0.5f), 100.0f);
            imgCertoErradoObj.GetComponent<Image>().sprite = Sprite.Create(imgErrado, new Rect(0.0f, 0.0f, imgErrado.width, imgErrado.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
        if (CardGenerate.cartaAtual.GetResposta() == false && PlayerPrefs.GetString("botaoResposta") == "true") {
            highlightObj.GetComponent<Image>().sprite = Sprite.Create(highlightErrou, new Rect(0.0f, 0.0f, highlightErrou.width, highlightErrou.height), new Vector2(0.5f, 0.5f), 100.0f);
            instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Errou!</color>";
            imgRespostaObj.GetComponent<Image>().sprite = Sprite.Create(imgFalse, new Rect(0.0f, 0.0f, imgFalse.width, imgFalse.height), new Vector2(0.5f, 0.5f), 100.0f);
            imgCertoErradoObj.GetComponent<Image>().sprite = Sprite.Create(imgErrado, new Rect(0.0f, 0.0f, imgErrado.width, imgErrado.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
        highlightObj.SetActive(true);
        imgCertoErradoObj.SetActive(true);
        imgRespostaObj.SetActive(true);

        buttonsTF.SetActive(false);
        botaoResposta.SetActive(false);        
        botaoProxCarta.SetActive(true);

    }

    public void proximaCarta() {
        PlayerPrefs.DeleteKey("botaoResposta");
        //CardGenerate.cartaAtual.GetResposta();
        PlayerPrefs.SetInt("cartaNumero", ((PlayerPrefs.GetInt("cartaNumero")) + 1));
        SceneManager.LoadScene("TelaCarta");
    }

    public void botaoSairDaPartida() {
        PlayerPrefs.DeleteKey("botaoResposta");
        PlayerPrefs.DeleteKey("seed");
        PlayerPrefs.DeleteKey("cartaNumero");
        SceneManager.LoadScene("TelaMenu");
    }


}
